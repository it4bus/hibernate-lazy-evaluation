package com.itersdesktop.javatechs.grails.hibernate

class TestController {

    def index() {
        println "starting controller"
        def ethan = Author.findByName('Ethan')
        println "author name is $ethan.name"
        def myBooks = ethan.books
        println "books are now stored in a variable"
        myBooks.each({println it.title})
        println myBooks.class.name
        render "OK!"
    }
}
