package com.itersdesktop.javatechs.grails.hibernate

class Author implements Serializable {
    String name
    static hasMany = [books: Book]
    static constraints = {
    }
}
