package com.itersdesktop.javatechs.grails.hibernate

class Book implements Serializable {
    String title
    static belongsTo = [author: Author]
    static constraints = {
    }
}
