import com.itersdesktop.javatechs.grails.hibernate.Author
import com.itersdesktop.javatechs.grails.hibernate.Book

class BootStrap {

    def init = { servletContext ->
        def ethan = new Author(name: 'Ethan').save()
        def book1 = new Book(title: 'Tuyet tinh coc')
        def book2 = new Book(title: 'Lua duc vong')
        ethan.addToBooks(book1)
        ethan.addToBooks(book2)
    }
    def destroy = {
    }
}
